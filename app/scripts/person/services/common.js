var personServiceCommon;
(function(){
    'use strict';
    var _fn = function($window){
        
        var db = new PouchDB('todos');
        var remoteCouch = false;

        var s = {};

        s.getWindowSelectedText = function(){
            return $window.getSelection().toString() ;
        }

        s.save = function(data, onSave, onError){
            data._id = new Date().toISOString();
            db.put(data, function callback(err, result) {
                if (!err) {
                    onSave();
                }else{
                    console.warn('save error', err);
                    onError(err);
                }
            });
        }

        return s;
    }; 
    
    personServiceCommon = _fn;

})();