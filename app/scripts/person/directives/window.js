var personWindow;
(function(){
    var _fn = function($rootScope, $comService, $sce, eventToggleOpen, $filter){
        
        var _ctrl = function($scope, $element, $attrs, $injector){
            
            $scope.isShow=false;
            $scope.data = {
                
            };
            $scope.error = null

            $scope.save = function(){
                var data = angular.copy($scope.data);
                $comService.save(data, function(){
                    
                    $scope.error=null;
                    $scope.data={};
                    $scope.isShow=!$scope.isShow;
                    $scope.$apply();

                }, function(err){
                    $scope.error = err;
                    $scope.$apply();
                })
            }

            $scope.getIsError = function(){
                return ($scope.error) ? true : false;
            }

            $scope.fillData = function(model){
                $scope.data[model] = $comService.getWindowSelectedText();
                if(model === 'dob' ){  

                    $scope.data[model] = $filter('personDateFilter')($scope.data[model]);
                    $scope.data[model] = $filter('date') ( new Date( $scope.data[model] ), 'yyyy-MM-dd' ) ;

                }
                
            }

            $rootScope.$on(eventToggleOpen, function(ev, data){
                $scope.error=null;
                $scope.data={};
                $scope.isShow=!$scope.isShow;                
            });
            
        };
        
        return {
            restrict: 'E',
            scope: {},
            controller: _ctrl,
            templateUrl: function(){
                return $sce.trustAsResourceUrl(chrome.extension.getURL('scripts/person/templates/window.html'))
            },
            link: function(scope, elm, attrs, ctrl) {
                
            }
        };
    };

    personWindow = _fn;

})();