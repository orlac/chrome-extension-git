var personButton;
(function(){
    var _fn = function($rootScope, $comService, $sce, eventToggleOpen){
        
        var _ctrl = function($scope, $element, $attrs, $injector){
            
            $scope.click = function(){
                $rootScope.$broadcast(eventToggleOpen, {});
                //alert( $window.getSelection().toString() );
            }
            
        };
        
        return {
            restrict: 'E',
            scope: {},
            controller: _ctrl,
            templateUrl: function(){
                return $sce.trustAsResourceUrl(chrome.extension.getURL('scripts/person/templates/button.html'))
            },
            link: function(scope, elm, attrs, ctrl) {
            
            }
        };
    }; 

    personButton = _fn;

})();