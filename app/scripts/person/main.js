window.addEventListener("load", function() {
//chrome.webNavigation.onDOMContentLoaded.addListener( function(){
	var mod = angular.module('person', []);

	var html = document.querySelector('html');
	html.setAttribute('ng-app', '');
	html.setAttribute('ng-csp', '');

	/** service */

	mod.factory('personServiceCommon', ['$window', function($window){
            return new personServiceCommon($window);
    }]);

	mod.constant('eventToggleOpen', 'eventToggleOpen' );

	/** filters */

	mod.filter('personDateFilter',function(){
        return new personDateFilter();
    });

	/** directives */

	mod.directive('personWindow', [
        '$rootScope'
        ,'personServiceCommon'
        ,'$sce'
        ,'eventToggleOpen'
        ,'$filter'
        ,personWindow
    ]);

	mod.directive('openButton', [
        '$rootScope'
        ,'personServiceCommon'
        ,'$sce'
        ,'eventToggleOpen'
        ,personButton
    ]);

    

	/** ini */

	var iniWindow = function(){
		var dir = document.createElement('person-window');
		html.appendChild(dir);
	}

	var nodeInsertedCallback = function(event) {
	    //console.log('DOMNodeInserted', event);
	    var test = document.querySelector('open-button');
	    if(!test){
	    	
	    	var bBar = document.querySelector('.aeH');
			var bParent = document.querySelector('.ar4');
			if( bBar && bParent ){
				var dir = document.createElement('open-button');
				bParent.insertBefore(dir, bBar);

				angular.bootstrap(html, ['person'], []);

				document.removeEventListener('DOMNodeInserted', nodeInsertedCallback);
			}

	    }
	};
	document.addEventListener('DOMNodeInserted', nodeInsertedCallback);

	iniWindow();

});