var personDateFilter;
(function(){
    var _fn = function(){
        
        return function(input){
            var d = Date.parse(input);
            if(!d){
                d = new Date().getTime();
            }
            return d;
        };
    };

    personDateFilter = _fn;

})();